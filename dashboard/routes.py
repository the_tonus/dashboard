""" Dashboard Routes """    
from masonite.helpers.routes import get, post

def routes(prefix='/dashboard'):
    return [
        get('/dashboard', '/dashboard.controllers.DashboardController@show'),
        get('/dashboard/login', '/dashboard.controllers.AuthenticationController@show'),
        get('/dashboard/logout', '/dashboard.controllers.AuthenticationController@logout'),
        post('/dashboard/login', '/dashboard.controllers.AuthenticationController@authenticate'),
    ]
