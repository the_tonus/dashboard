''' Dashboard Controller '''
from app.User import User
from config.database import DB
from masonite.request import Request

class DashboardController:

    def show(self, request: Request):
        if request.user() and request.user().is_admin:
            return view('/dashboard/templates/dashboard/index')
        
        if request.user():
            request.session.flash('danger', 'You are not an admin')
        return request.redirect('/dashboard/login')
    