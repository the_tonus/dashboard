""" Custom Links """
from .Link import BaseLink, UserLink
from masonite.request import Request


class Home(BaseLink):
    display = 'Home'
    url = '/dashboard'

class Logout(UserLink):
    display = 'Logout'
    url = '/logout'
